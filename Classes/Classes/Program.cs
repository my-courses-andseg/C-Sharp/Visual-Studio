﻿using System;
using Classes.Math;

namespace Classes
{
    public class Person
    {
        public string name;

        public void Introduction()
        {
            Console.WriteLine("Hello. My name is " + name);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Person andre = new Person();
            andre.name = "andre";
            andre.Introduction();
            Console.WriteLine(Calculator.Add(1, 2));
        }
    }
}
